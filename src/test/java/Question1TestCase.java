import org.junit.*;

/**
 * @author kowsercse@gmail.com
 */
public class Question1TestCase {

  private Question1 question1;

  @Before
  public void setUp() {
    question1 = new Question1();
  }

  @Test
  public void testGetMaxDrop() {
    final int actual = question1.getMaxDrop(2, 50);
    Assert.assertEquals("Max drop calculation is not correct", 10, actual);
  }

}
