import org.junit.Assert;
import org.junit.Test;

/**
 * @author kowsercse@gmail.com
 */
public class Question5Test {

  @Test
  public void testCountDigitAppearance() throws Exception {
    final Question5 question5 = new Question5();

    final int number = 900034;
    final long[] expected = new long[10];
    for (int i = 0; i <= number; i++) {
      int temp = i;
      while (temp > 0) {
        expected[temp % 10]++;
        temp /= 10;
      }
    }

    final long[] actual = question5.countDigitAppearance(number);
    Assert.assertArrayEquals("Digits does not match", expected, actual);
    for (int i = 0; i < actual.length; i++) {
      System.out.println("Total digit: " + i + " => " + expected[i] + " - " + actual[i]);
    }
  }

}