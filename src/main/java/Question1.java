/**
 * @author kowsercse@gmail.com
 */
public class Question1 {

  public int getMaxDrop(final int egg, final int floor) {
    int[][] table = new int[egg + 1][floor + 1];

    for (int i = 1; i <= floor; i++) {
      table[1][i] = i;
    }

    for (int i = 1; i <= egg; i++) {
      table[i][1] = 1;
    }

    for (int e = 2; e <= egg; e++) {
      for (int f = 2; f <= floor; f++) {
        int min = Integer.MAX_VALUE;

        for (int col = 1; col < f; col++) {
          final int max = Math.max(table[e - 1][col - 1], table[e][f - col]);
          if (min > max) {
            min = max;
          }
        }
        table[e][f] = 1 + min;
      }
    }

    return table[egg][floor];
  }

  public static void main(String[] args) {
    final Question1 question1 = new Question1();
    final int maxDrop = question1.getMaxDrop(2, 50);
    System.out.println("maxDrop = " + maxDrop);
  }

}
