/**
 * @author kowsercse@gmail.com
 */
public class Question5 {

  public long[] countDigitAppearance(final long number) {
    final int length = (int) Math.log10(number) + 1;
    final long[] maxDigit = intMaxDigits(length);
    final long[] extraZeroDigits = intExtraZeroDigits(length);

    final long totalDigit[] = new long[10];

    long n = number;
    for (int i = length; i > 0; i--) {
      long tens = (long) Math.pow(10, i - 1);
      final long reminder = n % tens;
      final int currentDigit = (int) (n / tens);

      if (currentDigit != 0) {
        for (int digit = 0; digit < 10; digit++) {
          long inc = maxDigit[i - 1] * currentDigit;
          totalDigit[digit] += inc;
        }
      }
      if (i >=2  && reminder / (int) Math.pow(10, i - 2) == 0) {
        totalDigit[0] -= extraZeroDigits[i - 1];
      }

      for (int digit = 1; digit < currentDigit; digit++) {
        totalDigit[digit] += tens;
      }

      totalDigit[currentDigit] += reminder + 1;
      n = reminder;
    }

    return totalDigit;
  }

  private long[] intExtraZeroDigits(final int length) {
    final long[] extraZeroDigit = new long[length];
    for (int i = 1; i < length; i++) {
      extraZeroDigit[i] = (long) Math.pow(10, i - 1);
    }
    return extraZeroDigit;
  }

  private long[] intMaxDigits(final int length) {
    final long[] maxDigit = new long[length];
    for (int i = 1; i < length; i++) {
      maxDigit[i] = (long) (Math.pow(10, i - 1) + 10 * maxDigit[i - 1]);
    }
    return maxDigit;
  }

  public static void main(String[] args) {
    final Question5 question5 = new Question5();

    System.out.println("To run from command line, use: java Question5 <number>");

    long limit = args.length == 0 ? System.currentTimeMillis() : Integer.valueOf(args[0]);
    System.out.println("Calculating total numbers of digit using: " + limit);

    final long[] digits = question5.countDigitAppearance(limit);
    for (int i = 0; i < digits.length; i++) {
      System.out.println("Total digit: " + i + " => " + digits[i]);
    }
  }

}
