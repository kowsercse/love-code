import java.math.BigDecimal;
import java.nio.CharBuffer;
import java.text.NumberFormat;
import java.util.*;

/**
 * @author kowsercse@gmail.com
 */
public class Question3 {
  public static String formatNumber(final String number) {
    if(number == null || number.length() == 0) {
      throw new IllegalArgumentException("Number can be empty or null");
    }

    final String[] parts = number.split("\\.");
    final String integerPart = parts.length == 0 ? number : parts[0];
    final String decimalPart = parts.length == 0 || parts.length == 1 || parts[1].length() == 0? "00" : parts[1];
    final StringBuilder stringBuilder = new StringBuilder();
    final int length = integerPart.length();
    for (int i = 1; i <= length; i++) {
      stringBuilder.append(integerPart.charAt(length - i));
      if (i % 3 == 0 && i != length) {
        stringBuilder.append(',');
      }
    }

    stringBuilder.reverse();
    stringBuilder.append('.');
    stringBuilder.append(decimalPart);
    return  stringBuilder.toString();
  }



  public static void main(String[] args) {
    final Scanner scanner = new Scanner(Question3.class.getResourceAsStream("/question3.in"));
    while (scanner.hasNext()) {
      System.out.print("Enter the number to format with comma: ");
      final String number = scanner.next();
      System.out.println("Processing: " + number);
      final String formattedNumber = formatNumber(number);
      System.out.println("Formatted Number = " + formattedNumber);

      final BigDecimal bigDecimal = new BigDecimal(number);
      final NumberFormat formatter = NumberFormat.getNumberInstance(Locale.US);
      final String javaFormatted = formatter.format(bigDecimal);
      System.out.println("Using Java: " + javaFormatted);
    }
  }

}
