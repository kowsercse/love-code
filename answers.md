## Question 1

```
cd src/main/java
javac Question1.java
java Question1
```

## Question 2
*What is your current home development environment like and what type of personal development projects are you working on?*

My home and work environment is same.

- Operating System: Ubuntu 14.04. Using linux from November 2010. Its already 5 years.
- Tools:
  - Java
  - Maven
  - Tomcat
  - Grails
  - MySQL
  - PHP
  - Git
  - NodeJS
  - Hadoop
  - With **sudo apt-get install package-name** anything get installed within a moment. So life is easier with Linux, specially ubuntu.
- IDE
  - Intellij Idea
    - Use it most of the time, others are used when necessary. Thanks to Jetbrains for their studnet license :-)
  - PHPStorm
  - WebStorm
  - Clion
  - Atom
- Laptop Configuration
  - RAM: 16GB
  - SSD: 512
  - Processor: Intel® Core™ i7-4720HQ CPU @ 2.60GHz × 8

- Macbook Pro: Used sometimes for iOS development
  - Processor: i5 dual core
  - Ram: 8GB
  - HDD: 512

Personally I am working on followin projects.

Currently developing an EMA application. It collects data from users randomly from user's predefined time. In a simple way, it is a survey tool, with a mobile client and java based web application.

Developed mobile application which uses camera to measure heartbit.
Also trying to measure user's pain level from facial expression by image processing. 

## Question 3

  ```
cd src/main/java
javac Question3.java
java Question3
  ```

## Question 4
*What is your dream development environment?*

- Business class laptop from Dell (preferable) / Lenovo
- Ram 32GB
- Intel i7 Quad Core Processor
- SSD 256 GB
- Dual monitor
- An ergonomic chair
- High Speed internet
- A good head phone


## Question 5

```
cd src/main/java
javac Question5.java
java Question5 123456
```
